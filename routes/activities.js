const express = require('express');
const moment = require('moment');
const router = express.Router();
const getLocations = require('../models/activities').getLocations;
const createLocation = require('../models/activities').createLocation;

/* GET home */
router.get('/', function (req, res, next) {
    res.render('index');
});

/* GET locations */
router.get('/resoltz', function (req, res, next) {
    getLocations()
        .then(locations => { 
            let sum = 0;
            let initialDistance = 0;
            let distance = locations.map((plot, index) => {
                initialDistance = locations[0].odometer;
                return sum += (index === 0 ? 0 : Math.abs(plot.odometer - locations[index - 1].odometer))
            })
            let distanceKm = distance[distance.length - 1] - initialDistance;
            distanceMiles = distanceKm*0.621371
            // distanceMiles = distanceKm*0.621371
            console.log(distanceKm)
            res.render('index', {
                moment,
                locations,
                distanceMiles
            });
        })
});

/* GET locations */
router.get('/map', function (req, res, next) {
    getLocations()
        .then(locations => {
            const locationData = []
            locations.map(location => {
                const locationObj = {}
                locationObj.lat = Number(location.latitude);
                locationObj.lng = Number(location.longitude);
                locationData.push(locationObj);
            })
            res.json(locationData);
        });
});

/* POST locations */
router.post('/resoltz', async function (req, res, next) {
    const locations = req.body
    console.log('received body: ', locations)

    try {
        returnedLocation = await createLocation(locations)
        console.log('we got the promise back. returnedLocation===> ', returnedLocation)  
    } catch (err) {
        console.log('we pulled a muscle! ', err)
        res.status(500).send({error: 'pulled a muscle: ' + err})
    }
    res.redirect('/resoltz');
});

module.exports = router;