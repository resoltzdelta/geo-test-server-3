module.exports = {
    development: {
        client: 'mssql',
        connection: {
            server: process.env.MSSQL_SERVER,
            user: process.env.MSSQL_USER,
            password: process.env.MSSQL_PASSWORD,
            database: process.env.MSSQL_NAME,
            options: {
                port: process.env.MSSQL_PORT,
                database: process.env.MSSQL_NAME,
                encrypt: true,
            }
        },
        searchPath: ['knex', 'public']
    },
    test: {
        client: 'pg',
        connection: process.env.PG_CONNECTION_STRING,
        searchPath: ['knex', 'public']
    },
    production: {
        client: 'mssql',
        connection: {
            server: process.env.MSSQL_SERVER,
            user: process.env.MSSQL_USER,
            password: process.env.MSSQL_PASSWORD,
            database: process.env.MSSQL_NAME,
            options: {
                port: process.env.MSSQL_PORT,
                database: process.env.MSSQL_NAME,
                encrypt: true,
            }
        },
        searchPath: ['knex', 'public']
    },
};
