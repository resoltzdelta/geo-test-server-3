const db = require('../services/dbservice');
console.log("========db from services========", db);
const createLocation = async params => {
    if (Array.isArray(params)) {
        for (let location of params) {
            await createLocation(location);
        }
        return;
    }
    const location = params.location;
    const device = params.device || {
        model: 'UNKNOWN'
    };
    const coords = location.coords;
    const battery = location.battery || {
        level: null,
        is_charging: null
    };
    const activity = location.activity || {
        type: null,
        confidence: null
    };
    const geofence = location.geofence ? JSON.stringify(location.geofence) : null;
    const provider = location.provider ? JSON.stringify(location.provider) : null;
    const extras = location.extras ? JSON.stringify(location.extras) : null;
    const now = new Date();
    const uuid = device.framework ? device.framework + '-' + device.uuid : device.uuid;
    const model = device.framework ? device.model + ' (' + device.framework + ')' : device.model;
    
    return db('distance').insert({
        uuid: location.uuid,
        company_token: params.company_token || null,
        device_id: uuid,
        device_model: model,
        latitude: coords.latitude,
        longitude: coords.longitude,
        accuracy: coords.accuracy,
        altitude: coords.altitude,
        speed: coords.speed,
        heading: coords.heading,
        odometer: location.odometer,
        event: location.event,
        activity_type: activity.type,
        activity_confidence: activity.confidence,
        battery_level: battery.level,
        battery_is_charging: battery.is_charging,
        is_moving: location.is_moving,
        geofence: geofence,
        provider: provider,
        extras: extras,
        recorded_at: location.timestamp,
        created_at: now
    }).returning('*')

}

const getLocations = () => {
    return db.select().from('distance')
}

module.exports = {
    createLocation,
    getLocations
};