const config = require('../app.config/dbconfig.js');
const knex = require('knex');

const environment = process.env.NODE_ENV || 'production';
console.log("====dbservice-environment====", environment);
const environmentConfig = config[environment];
const db = knex(environmentConfig)

module.exports = db;