

let coordinates = [];
    function myMap() {
        fetch('https://geo-test-server-3.azurewebsites.net/map', {
            method: 'GET',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            }
        })
        .then( response => {
            return response.json();
        })
        .then( coordinatesServer => {
            coordinates = coordinatesServer
            
            const mapProp = {
                zoom: 18,
                mapTypeId: 'terrain',
                center: {lat: coordinates[0].lat,lng: coordinates[0].lng},
            };
            const map = new google.maps.Map(document.getElementById('map'), mapProp);
            const flightPlanCoordinates = coordinates;
            const flightPath = new google.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            console.log("==========>", coordinates)
            flightPath.setMap(map);           
        });
}
